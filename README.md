# Zadání úkolů

1. Realizujte implementaci podle diagramu tříd.
2. Zapojte vlastní tvořivost. Zadání je naschvál neúplné a nechám na vás čím mě překvapíte.
3. Rozhraní “INázevRepository”
    + Budou implementovány do vlastních tříd např “NázevRepository”.
    + Dodržte konvenci v názvech. 
    + Při implementaci se snažte využít všechny znalosti z předchozích cvičení.
4. “NázevModel” 
    + Bude pouze připravovat data na uložení do “Repository”
5 “NázevController” 
    + Bude pouze validovat data. Snažte se pokrýt veškerou validaci, co vás napadne.
    + Všechny metody z rozhraní “INázevRepository” se propíšou přes “NázevModel” a následně do “NázevController”.
    + Jednotlivé akce v metodách zaznamenávejte do loggeru.
        + Např. uložení uživatele
        + Použijte java Logger z balíčku java.util.logging.Logger.
        + Každý kontrolér bude mít vlastní logger.
6. V metodě main vytvořte instance od kontrolérů a vložte 5 uživatelů a 10 zpráv.
7. Demonstrujte všechny metody z obou rozhraní. (Pouze 7 metod. Jedna metoda z rozhraní slouží pro validace. Zjistěte sami.)
8. Vypište oba loggery kontrolérů.


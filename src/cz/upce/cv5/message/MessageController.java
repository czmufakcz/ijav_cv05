package cz.upce.cv5.message;

import java.util.Collection;
import java.util.logging.Logger;

public class MessageController {

    private static final Logger LOGGER = Logger.getLogger(MessageController.class.getName());

    private final MessageModel messageModel;

    public MessageController() {
        this.messageModel = new MessageModel();
    }

    public Message save(Message message) {
        if (message == null) {
            String text = "Message is null.";
            LOGGER.warning(text);
            throw new NullPointerException(text);
        }

        if (message.getOwn() == null || message.getToUser() == null) {
            String text = "Own or toUser is null.";
            LOGGER.warning(text);
            throw new NullPointerException(text);
        }

        if (message.getMessage() == null || message.getMessage()
                                                   .isEmpty()) {
            String text = "Message is null or Messageis blank.";
            LOGGER.warning(text);
            throw new NullPointerException(text);
        }

        return messageModel.save(message);
    }

    public Message removeById(int id) {
        if (id < 0) {
            String message = "ID is invalid.";
            LOGGER.warning(message);
            throw new IllegalArgumentException(message);
        }

        Message messageRemoved = messageModel.removeById(id);

        if (messageRemoved == null) {
            String message = "Message not found.";
            LOGGER.warning(message);
            throw new IllegalArgumentException(message);
        }

        return messageRemoved;
    }

    public Collection<Message> getMessages(int page, int count) {
        if (count <= 0 || page <= 0) {
            String message = "Invalid arguments.";
            LOGGER.warning(message);
            throw new IllegalArgumentException(message);
        }

        return messageModel.getMessages(page, count);
    }

}

package cz.upce.cv5.message;
import java.util.Collection;

public interface IMessageRepository {
    
    Message save(Message message);
    
    Message removeById(int id);
    
    Collection<Message> getMessages(int page, int count);
}

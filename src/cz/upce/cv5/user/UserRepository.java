package cz.upce.cv5.user;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class UserRepository implements IUserRepository {

    private final List<User> users = new LinkedList<>();

    @Override
    public User save(User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findByUsername(String name) {
        return users.stream()
                    .filter(users -> name.equals(users.getUsername()))
                    .findFirst()
                    .get();
    }

    @Override
    public User removeByUsername(String name) {
        User removedUser = this.findByUsername(name);
        users.remove(removedUser);
        return removedUser;
    }

    @Override
    public boolean isExistUsername(String name) {
        return users.stream()
                    .anyMatch(user -> user.getUsername()
                                          .equals(name));
    }

    @Override
    public Collection<User> getAllUsers() {
        return users.stream()
                    .collect(Collectors.toList());
    }

}

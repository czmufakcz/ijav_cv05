package cz.upce.cv5.user;
import java.util.Collection;

public interface IUserRepository {

    User save(User user);

    User findByUsername(String name);

    User removeByUsername(String name);

    boolean isExistUsername(String name);

    Collection<User> getAllUsers();
}

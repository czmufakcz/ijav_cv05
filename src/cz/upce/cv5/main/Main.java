package cz.upce.cv5.main;

import cz.upce.cv5.message.Message;
import cz.upce.cv5.message.MessageController;
import cz.upce.cv5.user.ROLE;
import cz.upce.cv5.user.User;
import cz.upce.cv5.user.UserController;

public class Main {

    public static void main(String[] args) {
        UserController userController = new UserController();

        User user1 = new User("karel.novak", "password", 18, ROLE.USER);
        User user2 = new User("jaromir", "password", 35, ROLE.USER);
        User user3 = new User("bushman", "password", 28, ROLE.USER);
        User user4 = new User("jeliman", "password", 66, ROLE.USER);
        User user5 = new User("moris", "password", 88, ROLE.USER);

        userController.save(user1);
        userController.save(user2);
        userController.save(user3);
        userController.save(user4);
        userController.save(user5);

        MessageController messageController = new MessageController();
        messageController.save(new Message(user1, user2, "Message 1"));
        messageController.save(new Message(user1, user2, "Message 2"));
        messageController.save(new Message(user1, user2, "Message 3"));
        messageController.save(new Message(user1, user2, "Message 4"));
        messageController.save(new Message(user1, user2, "Message 5"));
        messageController.save(new Message(user1, user2, "Message 6"));
        messageController.save(new Message(user1, user2, "Message 7"));
        messageController.save(new Message(user1, user2, "Message 8"));
        messageController.save(new Message(user1, user2, "Message 9"));
        messageController.save(new Message(user1, user2, "Message 10"));

        messageController.getMessages(2, 5)
                         .forEach(System.out::println);
    }
}
